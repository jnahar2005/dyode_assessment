Instruction:-

1. Root directory of repository has the HTML for the mock-ups.
2. Folder "liquid-questions" has the following files with their details below:
 a. theory-question-answers.txt - has answers to the first two question which are theoretical.
 b. collection-pagination.liquid - has the answer to the 3rd question for pagination.
 c. product-variables.liquid - has the answer to the 4th question for product variables. 
 d. array-keys-values.liquid - has the answer to the 5th question related to array.